package xspeedit.exe.fr;

import java.util.HashMap;
import java.util.Map;

/**
 * Service d'emballage des articles
 *
 * @author Safae
 */
public class Emballage {
	
	/**
	 * Contient les articles
	 */
	private String currentContainer;
	/**
	 * Contient la somme de la taille des articles
	 */
	private int currentSize;

	/**
	 * Map tri�e par la taille des articles
	 */
	private Map<Integer, Long> articles = new HashMap<>();

	/**
	 * Emballe intelligemment les articles
	 *
	 * @param articlesEntree
	 */
	public void emballerArticles(String articlesEntree) {
		for (int i = 1; i <= 9; i++) {
			articles.put(i, compterArticles(articlesEntree, i));
		}
		System.out.println("Cha�ne d'articles emball�s: ");

		for (int i = 9, j = 1; i > 0; i--, j++) {
			while (articles.get(i) > 0) {
				if (articles.get(j) > 0) {
					// On stock 2 articles dans le paquet de taille 10 ex: 91/82
					System.out.print(i + "" + j + "/");
					articles.put(i, articles.get(i) - 1);
					articles.put(j, articles.get(j) - 1);
				} else {
					// On peut ajouter plusieurs articles dans le paquet tant que la taille du
					// nombre de paquet est inf�rieure � 10
					currentSize = i;
					currentContainer = i + "";
					articles.put(i, articles.get(i) - 1);
					ajouterAutreArticles(j);
					if (!currentContainer.isEmpty()) {
						System.out.print(currentContainer + "/");
					}
				}
			}
		}
	}

	/**
	 * Ajoute si possible un nouvel article dans l'emballage
	 *
	 * @param j
	 */
	private void ajouterAutreArticles(final int j) {
		for (int k = j; k > 0; k--) {
			if (articles.get(k) > 0 && currentSize + k <= 10) {
				currentContainer += k;
				currentSize += k;
				articles.put(k, articles.get(k) - 1);
				k++;
			}
		}
	}

	/**
	 * Compte le nombre d'articles de m�me taille
	 *
	 * @param entree
	 * @param i
	 * @return
	 */
	private Long compterArticles(final String entree, final int i) {
		return entree.chars().filter(carac -> carac == ('0' + i)).count();
	}

}
